api_id = "43b8590dac7cf8d902bdda6d2a413d0d403c6102";
api_secret = "721593ed466f77b830cc2a0117ebf94de6d10759";
console.log("rendered");
user_code = "";
user_token =
  "BAhbB0kiAbB7ImNsaWVudF9pZCI6IjQzYjg1OTBkYWM3Y2Y4ZDkwMmJkZGE2ZDJhNDEzZDBkNDAzYzYxMDIiLCJleHBpcmVzX2F0IjoiMjAxOC0wOC0xOVQxNTo0MjozMVoiLCJ1c2VyX2lkcyI6WzM2Mjg0OTI1XSwidmVyc2lvbiI6MSwiYXBpX2RlYWRib2x0IjoiMGU0NzA4NWE1NGNmNzY1NzA3ZGU0YjM0NGVjNTc0N2UifQY6BkVUSXU6CVRpbWUNb54dwLR//KkJOg1uYW5vX251bWkCOQE6DW5hbm9fZGVuaQY6DXN1Ym1pY3JvIgcxMDoJem9uZUkiCFVUQwY7AEY=--57151772eea49b34ec031f488cb4613e43dbec7e";
user_refresh =
  "BAhbB0kiAbB7ImNsaWVudF9pZCI6IjQzYjg1OTBkYWM3Y2Y4ZDkwMmJkZGE2ZDJhNDEzZDBkNDAzYzYxMDIiLCJleHBpcmVzX2F0IjoiMjAyOC0wOC0wNVQxNTo0MjozMVoiLCJ1c2VyX2lkcyI6WzM2Mjg0OTI1XSwidmVyc2lvbiI6MSwiYXBpX2RlYWRib2x0IjoiMGU0NzA4NWE1NGNmNzY1NzA3ZGU0YjM0NGVjNTc0N2UifQY6BkVUSXU6CVRpbWUNrxwgwLCL/KkJOg1uYW5vX251bWkCmAI6DW5hbm9fZGVuaQY6DXN1Ym1pY3JvIgdmQDoJem9uZUkiCFVUQwY7AEY=--ce2ec5bf1176a2f13fe22644b370625dc380605b";
var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split("&"),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
};

$(document).ready(function() {
  if (getUrlParameter("code")) {
    let code = getUrlParameter("code");
    user_code = code;
    callDicHead(code);
  } else {
    console.log("loda");
  }

  console.log("ready");
  clicked();
});

function clicked() {
  $("body").on("click", "#submit", function() {
    console.log("clicked");
    getAuth();
  });
}

function getAuth() {
  window.location.href = `https://launchpad.37signals.com/authorization/new?type=web_server&client_id=${api_id}&redirect_uri=https://trusting-thompson-cdda61.netlify.com`;
}

function callDicHead(code) {
  console.log("calling post");

  $.ajax({
    type: "POST",
    crossDomain: true,
    dataType: "json",
    url: `https://launchpad.37signals.com/authorization/token?type=web_server&client_id=${api_id}&redirect_uri=https://trusting-thompson-cdda61.netlify.com&client_secret=${api_secret}&code=${code}`
    // url: `_gettoken.json`
  })
    .done(function(data) {
      // console.log(data);
      saveToken(data);
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      // If fail
      console.log(textStatus + ": " + errorThrown);
    });
}

function saveToken(data) {
  user_token = data.access_token;
  user_refresh = data.refresh_token;
  console.log('token saved');
  getUserDetails();

}

function getUserDetails() {
    console.log("fetching user data");
  
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://launchpad.37signals.com/authorization.json",
        "method": "GET",
        "headers": {
          "authorization": "Bearer BAhbB0kiAbB7ImNsaWVudF9pZCI6IjQzYjg1OTBkYWM3Y2Y4ZDkwMmJkZGE2ZDJhNDEzZDBkNDAzYzYxMDIiLCJleHBpcmVzX2F0IjoiMjAxOC0wOC0xOFQwNzozODoxNloiLCJ1c2VyX2lkcyI6WzM2Mjg0OTI1XSwidmVyc2lvbiI6MSwiYXBpX2RlYWRib2x0IjoiMGU0NzA4NWE1NGNmNzY1NzA3ZGU0YjM0NGVjNTc0N2UifQY6BkVUSXU6CVRpbWUNR54dwB0kBJkJOg1uYW5vX251bWkCbQM6DW5hbm9fZGVuaQY6DXN1Ym1pY3JvIgeHcDoJem9uZUkiCFVUQwY7AEY=--9eca63cd12c7d607c219feae23229b4fed0f0cf6",
          "cache-control": "no-cache",
        }
      }
      
      $.ajax(settings).done(function (response) {
        console.log(response);
      });
  }
